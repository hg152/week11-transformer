# Builder stage
FROM rust:1.72 as builder
WORKDIR /usr/src/transformer
COPY . .
RUN apt-get update && apt-get install -y python3-venv python3-pip && rm -rf /var/lib/apt/lists/*
RUN pip3 install torch==2.1.0+cpu --index-url https://download.pytorch.org/whl/cpu --break-system-packages
ENV LIBTORCH_USE_PYTORCH=1
RUN cargo build --release

# Final stage
FROM debian:latest

RUN apt-get update && apt-get install -y libfontconfig1 wget python3 python3-pip && \
    pip3 install torch==2.1.0+cpu --index-url https://download.pytorch.org/whl/cpu --break-system-packages && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/src/transformer/target/release/transformer /usr/local/bin/transformer

# Download model files
RUN mkdir -p /usr/local/share/transformer_files && \
    wget -O /usr/local/share/transformer_files/config.json https://huggingface.co/sshleifer/distilbart-cnn-6-6/resolve/main/config.json?download=true && \
    wget -O /usr/local/share/transformer_files/vocab.json https://huggingface.co/sshleifer/distilbart-cnn-6-6/resolve/main/vocab.json?download=true && \
    wget -O /usr/local/share/transformer_files/merges.txt https://huggingface.co/sshleifer/distilbart-cnn-6-6/resolve/main/merges.txt?download=true && \
    wget -O /usr/local/share/transformer_files/rust_model.ot https://huggingface.co/sshleifer/distilbart-cnn-6-6/resolve/main/rust_model.ot?download=true

ENV LD_LIBRARY_PATH=/usr/local/lib/python3.11/dist-packages/torch/lib:$LD_LIBRARY_PATH
CMD ["/usr/local/bin/transformer"]






