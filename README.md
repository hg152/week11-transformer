# Week11 Transformer

This project is a rust based container that serves a distilbart-cnn-6-6 summarization model. The container is also deployed to AWS lambda.

## Tools and Technologies

- **Rust**: A systems programming language that guarantees memory safety and offers great performance.
- **rust-bert**: A rust library that provides end-to-end NLP pipelines.
- **Docker**: A set of platform-as-a-service products that use OS-level virtualization to deliver software in packages called containers.
- **Hugging face**: A repository containing source of ML models.
- **AWS Elastic Container Registry (ECR)**: A fully managed Docker container registry that makes it easy for developers to store, manage, and deploy Docker container images.
- **AWS Lambda**: A fully managed serveless function registry rhat makes it easy for developers to deploy their serverless functions in the AWS cloud.

## Running locally

Tp run this application directly in the local environment, you'll need to have Rust and Cargo installed.


1. **Clone the Repository**

    ```
    git clone https://your-repository-url.git
    cd your-project-directory
    ```

2. **Run directly**
    This command starts the application and exposes it on port 8080 of your localhost

    ```
    cargo run
    ```

## Running Containerized Locally

To run the containerized version of this application locally, you'll need to have Rust, Docker, and Cargo installed on your system.

1. **Clone the Repository**

    ```
    git clone https://your-repository-url.git
    cd your-project-directory
    ```

2. **Build the Docker Image**

    Navigate to the root of your project directory and run:

    ```
    docker build -t transformer .
    ```

4. **Run the Container**

    This command starts the container and exposes it on port 8080 of your localhost:

    ```
    docker run -p 9001:9001 transformer
    ```

## Running On AWS

This project can be deployed on AWS through AWS ECR and AWS Lambda. AWS ECR is a container registry that is used to store built docker image in our case. AWS Lambda is a convinient tool of deploying serveless functions in the AWS environment. 

1. **Clone the Repository**

    You need to first make a fork of this project in your own gitlab environment.

2. **Set Up AWS ECR**

    You'll need to first create a AWS ECR Repo:

    ![Alt text](https://i.imgur.com/ugQ9hF7.png)


3. **Build the Docker Image and upload to AWS ECR**

    First, we need to log into AWS from our local Docker:
    ```
    aws ecr get-login-password --region <primary region> | docker login --username AWS --password-stdin <account number>.dkr.ecr.us-east-1.amazonaws.com
    ```
    Then, we need to build the target docker image:
    ```
    docker build -t transformer .
    ```
    Then, we need to tag the image:
    ```
    docker tag transformer:latest <account number>.dkr.ecr.us-east-1.amazonaws.com/transformer:latest
    ```
    Lastly, we push the image to the ECR Repo:
    ```
    docker push <account number>.dkr.ecr.us-east-1.amazonaws.com/transformer:latest
    ```

4. **Set up the AWS Lambda Function**

    Set up the AWS Lambda function and configure it to use the Docker image that we uploaded to ECR. Given that ML inference is a memory consuming task, I'd suggest modify the configuration to allow more memory usage.

    ![Alt text](https://i.imgur.com/LHUILqd.png)

## Using the Application

Once a version of this application is running, we can then test the functionality of the application.

### Run Transformer

This application can be used to summarize a body of text. We can create a test event in AWS Lambda with the following body attached: 
```
{
  "text": "In findings published Tuesday in Cornell University's arXiv by a team of scientists from the University of Montreal and a separate report published Wednesday in Nature Astronomy by a team from University College London (UCL), the presence of water vapour was confirmed in the atmosphere of K2-18b, a planet circling a star in the constellation Leo. This is the first such discovery in a planet in its star's habitable zone — not too hot and not too cold for liquid water to exist. The Montreal team, led by Björn Benneke, used data from the NASA's Hubble telescope to assess changes in the light coming from K2-18b's star as the planet passed between it and Earth. They found that certain wavelengths of light, which are usually absorbed by water, weakened when the planet was in the way, indicating not only does K2-18b have an atmosphere, but the atmosphere contains water in vapour form."
}
```
This specifies the body of text we want it to summarize. The lambda will return the following result:
```
{
    "summary": "" The presence of water vapour was confirmed in the atmosphere of K2-18b, a planet circling a star in the constellation Leo. This is the first such discovery in a planet in its star's habitable zone. The Montreal team used data from the NASA's Hubble telescope to assess changes in the light coming from K2""
}
```

![Alt text](https://i.imgur.com/vDozbie.png)
