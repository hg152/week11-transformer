use lambda_runtime::{service_fn, Error, LambdaEvent};
use rust_bert::pipelines::common::ModelResource;
use rust_bert::pipelines::common::ModelType::Bart;
use rust_bert::pipelines::summarization::{SummarizationConfig, SummarizationModel};
use rust_bert::resources::LocalResource;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;

#[derive(Deserialize)]
struct Request {
    text: String,
}

#[derive(Serialize)]
struct Response {
    summary: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(summarization);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn summarization(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let config_resource = Box::new(LocalResource {
        local_path: PathBuf::from("usr/local/share/transformer_files/config.json"),
    });

    let vocab_resource = Box::new(LocalResource {
        local_path: PathBuf::from("usr/local/share/transformer_files/vocab.json"),
    });

    let model_resource = Box::new(LocalResource {
        local_path: PathBuf::from("usr/local/share/transformer_files/rust_model.ot"),
    });

    let merges_resource = Box::new(LocalResource {
        local_path: PathBuf::from("usr/local/share/transformer_files/merges.txt"),
    });

    let model_config: SummarizationConfig = SummarizationConfig {
        model_type: Bart,
        model_resource: ModelResource::Torch(model_resource),
        vocab_resource: vocab_resource,
        merges_resource: Some(merges_resource),
        config_resource: config_resource,
        ..Default::default()
    };

    let summarization_model = SummarizationModel::new(model_config).unwrap();

    let text = event.payload.text;
    let input = [text];
    let output = summarization_model.summarize(&input);

    Ok(Response {
        summary: format!("{}", output.unwrap()[0]),
    })
}